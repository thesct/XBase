/**************************************

    author  : xy_god
    e-mail  : xy_god@thesct.net
    homepage: http://www.thesct.net

    FileName: mem_pool.h
    gen_time: 2014.10.13

    comments:
        该文件定义了内存池的基本数据结构
    并声明了其所具有的相应操作.

 **************************************/

#ifndef MEM_POOL_H
#define MEM_POOL_H

//-------------------------------- start --------------------------------

// 错误值定义

#define ERR_MP_PARAM -1                 // 参数不合法
#define ERR_MP_NOMEM -2                 // 内存不足
#define ERR_MP_NOOBJ -3                 // 预定操作对象不存在

// 该值须>=(目标平台上字长或者指针长度中的较大者),并且,最好是字长的自然数倍
#ifndef UNIT_LEN
#define UNIT_LEN 8
#endif

typedef char UNIT_TYPE[UNIT_LEN];

// 每个内存块中,内存单元的个数
#ifndef UNIT_NUMBER
#define UNIT_NUMBER 256
#endif

// 内存块结构
typedef struct x_mem_block
{
    UNIT_TYPE unit_array[UNIT_NUMBER];    // 内存单元数组  
    struct x_mem_block *p_next;           // 下一节点
} x_mem_block_t;


// 内存池结构
typedef struct x_mem_pool
{
  x_mem_block_t *p_block;            // 内存块链表头

  x_mem_block_t *p_mem_free_list;    // 空闲内存列表
  x_mem_block_t *p_mem_used_list;    // 在用内存列表
} x_mem_pool_t;

// 函数声明
x_mem_pool_t *create_mem_pool(int *p_errno);
void *mem_alloc(x_mem_pool_t *p_mem_pool,int i_size, int *p_errno);
int mem_free(x_mem_pool_t *p_mem_pool, void *p_addr_req);
int mem_pool_reset(x_mem_pool_t *p_mem_pool);
int destroy_mem_pool(x_mem_pool_t *p_mem_pool);

int mem_pool_print(x_mem_pool_t *p_mp, int b_only_total);

//--------------------------------  end  --------------------------------

#endif // <<< MEM_POOL_H
