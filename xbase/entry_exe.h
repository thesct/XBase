/**************************************

    author  : xy_god
    e-mail  : xy_god@thesct.net
    homepage: http://www.thesct.net

    FileName: entry_exe.h
    gen_time: 2015.01.22

    comments:
        该文件定义了声明了EXE文件中的主线程
    函数。

 **************************************/

#ifndef ENTRY_EXE_H
#define ENTRY_EXE_H

#include "xbase.h"
#include "xmem_pool.h"
#include "xthread.h"

//-------------------------------- start --------------------------------

typedef struct x_cmd_line
{
  int count;
  char **args;
} x_cmd_line_t;

// 获取命令行参数
x_cmd_line_t get_cmd_line();

// 线程函数指针类型
void * t_main(const x_local_obj_t *p_local_obj,void *args);

// 函数声明


//--------------------------------  end  --------------------------------

#endif // <<< ENTRY_EXE_H
