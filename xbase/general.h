/**************************************

    author  : xy_god
    e-mail  : xy_god@thesct.net
    homepage: http://www.thesct.net

    FileName: general.h
    gen_time: 2014.12.23

    comments:
        该文件包含了所有的XBase库的头
    文件，要使用XBase库，只需包含这个
    头文件就可以了。

 **************************************/

#ifndef GENERAL_H
#define GENERAL_H

//-------------------------------- start --------------------------------
#include "./xbase.h"
#include "./xmem_pool.h"
#include "./xthread.h"

// 函数声明


//--------------------------------  end  --------------------------------

#endif // <<< GENERAL_H
