/**************************************

    author  : xy_god
    e-mail  : xy_god@thesct.net
    homepage: http://www.thesct.net

    FileName: thread.h
    gen_time: 2015.01.22

    comments:
        该文件定义了线程的基本数据结构
    并声明了其所具有的相应操作.

 **************************************/

#ifndef X_THREAD_H
#define X_THREAD_H

#include <pthread.h>

#include "xbase.h"
#include "xmem_pool.h"

//-------------------------------- start --------------------------------

// 线程本地对象结构
typedef struct x_local_obj
{
  x_mem_pool_t *p_mp; // 内存池对象指针
} x_local_obj_t;

// 线程函数指针类型
typedef void* (*x_thread_func_t)(const x_local_obj_t *p_local_obj,void *args);

// 函数声明

// 创建线程
int create_thread(pthread_t *thread, const pthread_attr_t *attr,
				  x_thread_func_t thread_func, void *args);

//--------------------------------  end  --------------------------------

#endif // <<< X_THREAD_H
