/**************************************

    author  : xy_god
    e-mail  : xy_god@thesct.net
    homepage: http://www.thesct.net

    FileName: xbase.h
    gen_time: 2015.01.12

    comments:
        xbase.c的头文件

 **************************************/

#ifndef XBASE_H
#define XBASE_H

#include <stdio.h>
#include <stdlib.h>

//-------------------------------- start --------------------------------
#define ERR_XB_REUNINIT -998                 // XBase库重复清理
#define ERR_XB_REINIT -999                 // XBase库重复初始化

// 函数声明
int xbase_is_initialized();

int xbase_init();
int xbase_uninit();

void * mallocs(size_t size);
void frees(void *ptr);

//--------------------------------  end  --------------------------------

#endif // <<< XBASE_H
