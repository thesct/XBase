
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "entry_exe.h"

void init_cmd_line(int argc, char *argv[]);

// 可执行文件的入口点
int main(int argc, char *argv[])
{

  int i_err = 0;

  init_cmd_line(argc, argv);
  
  i_err = xbase_init();
  if (i_err != 0) {
    printf("main函数中，执行xbase_init后发生错误!错误号：%d.\r\n", i_err);
    goto error_tray;
  }

  x_mem_pool_t * p_mp = NULL;
  p_mp = create_mem_pool(&i_err);
  if (p_mp == NULL) {
    printf("main函数中，执行create_mem_pool后发生错误!错误号：%d.\r\n", i_err);
    goto error_tray;
  }

  x_local_obj_t local_obj;
  local_obj.p_mp = p_mp;
  
  t_main(&local_obj, NULL);
  
  // 正常退出
  if (local_obj.p_mp != NULL) {
	destroy_mem_pool(local_obj.p_mp);
  }
  xbase_uninit();
  return 0;
 error_tray:  // 错误处理
  if (local_obj.p_mp != NULL) {
	destroy_mem_pool(local_obj.p_mp);
  }  
  xbase_uninit();
  return i_err;
}

inline x_cmd_line_t* get_cmd_line_v()
{
  static x_cmd_line_t cmd_line;
  return &cmd_line;
}


x_cmd_line_t get_cmd_line()
{
  x_cmd_line_t cmd_line;
  cmd_line = *(get_cmd_line_v());
  return cmd_line;
}

void init_cmd_line(int argc, char *argv[])
{
  x_cmd_line_t *p_cmd_line = get_cmd_line_v();
  p_cmd_line->count = argc;
  p_cmd_line->args = argv;
}
