#include "./xbase/general.h"
#include "./xbase/entry_exe.h"

#include <stdio.h>
#include <string.h>

void * t_main(const x_local_obj_t *p_local_obj,void *args)
{
  printf("t_main函数,XBase可执行文件的入口点!\r\n");

  static int i_ret = 0;
  
  x_cmd_line_t cmd_line;
  x_mem_pool_t * p_mp = NULL;


  if(p_local_obj == NULL) {
    printf("线程本地对象未准备好,XBase系统异常.\n");
	i_ret = -1;
	return &i_ret;
  }
  p_mp = p_local_obj->p_mp;
  
  // 获取命令行参数
  cmd_line = get_cmd_line();
  int i = 0;
  for (i = 0; i < cmd_line.count; i++) {
	printf("第%d个命令行参数是: %s\r\n" ,i+1, cmd_line.args[i]);
  }

  /**/
  int i_err = 0;
  void * p = mem_alloc(p_mp, 4, &i_err);
  if (p == NULL) {
	printf("分配内存失败,错误号:%d\n", i_err);
  }
  char * c = (char*)p;
  strcpy(c, "abc");
  printf("the string is : %s\n", c);
  /**/

  p = mem_alloc(p_mp, 16, &i_err);
  mem_free(p_mp, p);
  mem_alloc(p_mp, 32, &i_err);
  
  printf("按回车键退出！\r\n");
  getchar();
  mem_pool_print(p_mp, 0);
  i_ret = 0;
  return &i_ret;
}

